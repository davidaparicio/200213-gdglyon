+++
weight = 7
+++
{{% section %}}

## Au final
![List](img/jira/list.png)

---
## Pour aller plus loin

* [Sophia Security Camp 2019](http://www.telecom-valley.fr/sophia-security-camp-2019/)
* [ANSSI Sécurité Agile](https://www.ssi.gouv.fr/administration/guide/agilite-et-securite-numeriques-methode-et-outils-a-lusage-des-equipes-projet/) (Atelier d'analyse de risque)
![Guide de l'ANSSI, Agilité & Sécurité numériques](img/anssi_agile.jpg)

---
## Analogie
> « Nul n'est censé ignorer la loi »

---
## Ma devise
> « Nul développeur n'est censé ignorer la sécurité »

---
## David Aparicio


15/ DD INSA de Lyon / UNICAMP (Brésil)
Facebook Open Academy / MIT AppInventor

17/ Dev(Sec)Ops @ AMADEUS (Nice, 2 ans)

19/ DataOps @ OVHCloud (Lyon)

<!-- Curieux
* Transactional Memories
* Battery Profiling
* MIT AppInventor
* SMPC / BC -->
---
## Merci
![oscar](img/thanks.gif)

---
## Questions ?
![chien](img/security_container_small.gif)

{{% /section %}}

---
#### Pas de MEP -> Failure Fridays

![DevOps](img/misfortune.png)

[PagerDuty, 121, 200 tickets opened, 3 full AZ failures](https://www.pagerduty.com/blog/failure-fridays-four-years/)

<!--
#### Ex: Maturité des équipes
![Maturity Matrix](img/maturity.png)
---
-->
