+++
weight = 1
+++
## Security By Design: Cas concret avec la sauvegarde automatisée de Kerberos
[David Aparicio](https://david.aparicio.eu)

*DevOps Cloud and Bigdata*

###### GDG Cloud and IoT Lyon (13 Février 2020)

<!--
---
{{< slide id="disclaimer" background="#9a0007" transition="page">}}
## Avant propos
L'ensemble de la présentation porte sur ma mission DevOps Bigdata ALTRAN chez AMADEUS

Les opinions sont les miennes et non celles de mon ancien employeur, ni de mon employeur actuel
##### Disclaimer
-->
