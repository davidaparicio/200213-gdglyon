+++
weight = 5
+++
#### Où stocker la sauvegarde ?

{{% fragment %}} * Git {{% /fragment %}}
{{% fragment %}} * NFS {{% /fragment %}}
{{% fragment %}} * HDFS 
(Hadoop Distributed File System)
{{% /fragment %}}

<!--___
## Architecture
* GMock
* Master
* Worker
* Gateway
* Consul KV -->
___

{{% section %}}
#### Architecture générale
![Architecture](img/archi_gmock.png)
___
#### Résultat
![Architecture avec la sauvegarde](img/archi_gmock2.png)

{{% /section %}}

---
{{% section %}}
## SSH
![StackOverFlow](img/sof/question.png)

___
#### Réponse 98%
![Réponse](img/sof/nosecure.png)

___
#### Impatience
![Réponse](img/sof/queensofthestoneage.png)

___
#### Réponse correcte
![Réponse](img/sof/answer.png)

___
## Solution
![Solution](img/sof/solution.png)

{{% /section %}}

---
{{% section %}}
## Tests
Que se passe-t'il lorsqu'on sauvegarde/restaure?

Comment les jobs Map/Reduce ou Spark se comportent pendant l'absence/build du GMock ?

---
#### Après les Unit Tests, Chaos Monkey
![Chaos](img/chaos.jpg)

---
## QA
![QA](img/qa.jpg)

---
#### Détection d'un effet de bord
![QA](img/jira/qa.png)

{{% /section %}}
