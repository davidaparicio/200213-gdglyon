+++
weight = 2
+++
{{% section %}}

<!--
### Intro
![Covid-19](img/coronavirus.jpg)
---
#### La nuit tous les hackers sont gris
![hacker](img/hacker.jpg)
---
-->
### Sécurité dès la conception

95/ SdD-"Privacy By Design"

97/ Loi allemande

10-12/ Congrès

---
### DevSecOps

![DevSecOps](img/devsecops.png)
[dodcio.defense.gov](https://dodcio.defense.gov/Portals/0/Documents/DoD%20Enterprise%20DevSecOps%20Reference%20Design%20v1.0_Public%20Release.pdf?ver=2019-09-26-115824-583)

---
### Pas copier-coller `StackOverFlow`

98% snippets sur la sécurité/crypto
sont *insecures*

Fisher et al., 2017; Nadi et al., 2016; Das et al, 2014
[Prevent cryptographic pitfalls by design](https://archive.fosdem.org/2019/schedule/event/crypto_pitfalls/)

---
#### Attention avec Docker
![Docker vulnerabilities](img/snyk_rapport.png)
[The state of open source security – 2019](https://snyk.io/opensourcesecurity-2019/)

---
#### Attention avec vos dépendences
![Dependencies vulnerabilities](img/snyk_dep.png)
[The state of open source security – 2019](https://snyk.io/opensourcesecurity-2019/)

{{% /section %}}

---
## Bonnes pratiques

{{% fragment %}}* Principe de moindre privilège !root {{% /fragment %}}
{{% fragment %}}* Diminuer surface d'attaque (scratch, distroless){{% /fragment %}}
{{% fragment %}}* Pas de secrets dans les Docker images {{% /fragment %}}
{{% fragment %}}* Pas de données sensibles dans les GUI{{% /fragment %}}
{{% fragment %}}* Mettre à jour infra/docker images (CI/CD|[GitOps](https://www.infoq.com/news/2020/02/wksctl-kubernetes-gitops/)) {{% /fragment %}}
{{% fragment %}}* Ne pas afficher de stacktrace (pas debug) {{% /fragment %}}
{{% fragment %}}* Ni de version/nom de framework {{% /fragment %}}
{{% fragment %}}* Vérifier les entrées/sorties (injection/XSS) {{% /fragment %}}
{{% fragment %}}* PaaS (BUILD/RUN) 🇪🇺 OVHCloud/CleverCloud {{% /fragment %}}

---
![OWASP](img/keys.png)

---
## Prendre du recul /code
![C](img/code_c.png)

---

> «David, c'est quand que tu vas mettre des paillettes dans ma vie ?»

---
## Outils

{{% fragment %}}[Linter](https://en.wikipedia.org/wiki/Lint_(software))/[npm-audit](https://docs.npmjs.com/auditing-package-dependencies-for-security-vulnerabilities) (Code),
[SonarQube](https://www.sonarqube.org/) (Quality),
[Gitlab SAST](https://docs.gitlab.com/ee/user/application_security/sast/)/[Argo](https://argoproj.github.io/)/[GitHub](https://github.com/features/security) (CI/CD),
[Clair](https://coreos.com/clair/docs/latest/)/[Anchore](https://anchore.com/)/[Dagda](https://github.com/eliasgranderubio/dagda) (CVE),{{% /fragment %}}
{{% fragment %}}[OpenSCAP](https://www.open-scap.org/) (Audit),
[Cilium](https://cilium.io/) (Network),
[gVisor](https://github.com/google/gvisor)/[Kata](https://katacontainers.io/) (Sandbox),
[Istio](https://istio.io)/[maesh](https://containo.us/maesh/) (SSL),
[Notary](https://docs.docker.com/notary/getting_started/) (Sign.),{{% /fragment %}}
{{% fragment %}}[Falco](https://sysdig.com/opensource/falco/) (Monitoring K8s),
[42Crunch](https://42crunch.com/api-security/) (API Scanner),
[Burp Suite](https://portswigger.net/burp) (Vuln.Web),
[OWASP ZAP](https://www.zaproxy.org/) (Proxy),{{% /fragment %}}
{{% fragment %}}[GitGuardian](https://www.gitguardian.com/) (Secret),
[Kali](https://www.kali.org/) (OS),
[YesWeHack](https://www.yeswehack.com/) (Bounty){{% /fragment %}}

---

{{% section %}}
## Sonar
![Sonar](img/sonar.png)

---
## CI/CD
![Gitlab](img/pipeline.jpg)

---
## Falco
![Falco](img/falco.png)
[Kris Nova, Fixing the Kubernetes clusterfuck @FOSDEM](https://fosdem.org/2020/schedule/event/kubernetes/)

---
## Why?

![OWASP](img/owasp.png)
[OWASP Top 10](https://tinyurl.com/api-owasp)

---
#### Gendarmerie Nationale

> L’entrée en vigueur du [RGPD](https://fr.wikipedia.org/wiki/R%C3%A8glement_g%C3%A9n%C3%A9ral_sur_la_protection_des_donn%C3%A9es) modifie la posture des acteurs (des traitements) qui doivent tenir compte des impératifs de sécurité dès la conception d’un produit ainsi que son cycle de vie. Le label [« by design »](https://www.gendarmerie.interieur.gouv.fr/crgn/content/download/1033/16080/) devient un label de qualité qui constituera un atout commercial.

---
## 2022

* 90% des développements logiciels se déclaront DevSecOps (+40% 2019)
* 25% des développements IT selon DevOps (+10% 2019)

[Gartner/Techwire](https://www.techwire.net/sponsored/integrating-security-into-the-devsecops-toolchain.html)

___
#### Et pour éviter cela
![Windows 3.1](img/why_sec.png)

---
#### Ou ceci
![Crypto](img/cyber.jpg)

---
#### Snyk TL;DR
![TL;DR](img/snyk_takeaways.png)

{{% /section %}}
