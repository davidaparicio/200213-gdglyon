+++
weight = 3
+++
{{% section %}}
# ReX @AMADEUS

---
## Ce n'est pas ça
![Mozart](img/amadeus/mozart.jpg)

---
## Mais plutôt ceci
![Cryptic](img/amadeus/cryptic.png)

---
<!-- 
![2016](img/amadeus/history2016.jpg)
---
-->
## AMADEUS
* 30 ans
* Au début, 3 (AF, Lufthansa, Iberia)
* 700 compagnies aériennes / 50%
* Le voyageur au coeur du métier
* 1 an avant jusqu'au décollage

---
<!-- 
#### Passager first
![Passager](img/amadeus/passager_first.jpg)
---
-->
#### A la loupe (2015)
![2015](img/amadeus/figures.jpg)

<!-- 
[Top 10 trends travel transformation in 2020](https://youtu.be/nEB8mn7ujy8)
-->

---
#### BigData
![Eco](img/bigdata.png)

---
#### Usecase
![Overview](img/biguse.png)

---
#### Cluster PoC en Prod?
![RPi](img/cluster.jpg)

---
## 2018
* OVH->On-Premise->GCP
* MapR, Puppet 3, RHEL
* 200 noeuds Hadoop

---
## Specs
* 9800 Cores
* 65 TB RAM
* 4500 disks
* 6/13 PB
* 1-3 HDD/week

---
## Infos
* SSP/IHG/Kayak
* 120 000 TPS (peak)
* Crash&Burn->DEV->QA->UAT->PROD
* 3h du matin
* Environnement sécurisé (PCI-DSS)

{{% /section %}}
