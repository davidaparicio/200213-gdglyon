+++
weight = 4
+++
{{% section %}}
## Big Data
![Architecture](img/archi_gmock.png)

---
## Contexte
Kerberos est un SPOF (Point individuel de défaillance)

Pas de ressource pour le rendre HA (Haute Disponibilité)

Vite fait & Bien fait

---
## Story telling
![Pourquoi la sécurité](img/jira/daemon.png)

---
## Why?
![Outage](img/amadeus/outage.png)

---
#### Kerberos: kézako?
![Kerberos](img/kerberos1.png)
<!-- KDC = Key Distribution Center -->
Source (EN): [Kerberos pour les manageurs](https://dzone.com/articles/introduction-to-kerberos-for-managers)

---
#### Authentification | Autorisation
![Authentication/Authorization](img/kerberos2.png)

---
#### Kerberos++
Cours du MIT (EN):

[Fall 2014 Lecture 13: Kerberos](https://youtu.be/bcWxLl8x33c)

{{% /section %}}
