+++
weight = 6
+++
{{% section %}}
#### Modèle de menace/Threat model
Qui sont nos `masters` ? 😈

---
#### Single/🔒 Source of Truth
Puppet Master avec la gestion des certificats

[(SSOT)](https://en.wikipedia.org/wiki/Single_source_of_truth)

{{% /section %}}

---
{{% section %}}
#### Utilisateur applicatif
![Area](img/area.jpg)

---
## Documentation
![Doc](img/jira/doc.png)

---
## Configuration optionnelle
![Customization](img/jira/custom.png)

{{% /section %}}

---
### Quelques (autres) problèmes
{{% fragment %}}* Rotation backup pour éviter pb disque fatal {{% /fragment %}}
{{% fragment %}}* Chiffrement backup mot de passe + sel UUID {{% /fragment %}}
{{% fragment %}}* Cluster sans Kerberos (MapR ticket) {{% /fragment %}}
{{% fragment %}}* Pas de 50/50 avec le PO {{% /fragment %}}
{{% fragment %}}* != Sécurité ? {{% /fragment %}}
{{% fragment %}}* Temps de livraison / Story point {{% /fragment %}}
{{% fragment %}}* Accompagnement du Management {{% /fragment %}}
{{% fragment %}}* Parler ROI / RGPD (4% CA) / BigData {{% /fragment %}}
