# Security By Design: Cas concret avec la sauvegarde automatisée de Kerberos

## 20-02-13 : GDG Cloud and IoT Lyon

Kerberos est notre système d’authentification dans notre architecture BigData et dans cette architecture ce serveur est un SPOF (Single Point of Failure). 

Pour assurer la disponibilité de la plateforme j'ai mis en place un mécanisme de sauvegarde et restauration des informations relatives à Kerberos. Cela permet recréer au plus vite (en cas d'incident) un serveur Kerberos opérationnel.  
Ces informations étant critiques, la sécurité fût pensée dès la réalisation. 

Je présenterai les techniques utilisées et j'aborderai aussi la prise de conscience du management sur ces aspects importants. En effet, même en étant dans le réseau interne d’un datacenter, nous ne sommes pas à l’abri d’une intrusion. Le risque 0 n'existe pas.
